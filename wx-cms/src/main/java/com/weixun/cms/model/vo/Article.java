package com.weixun.cms.model.vo;


public class Article {
    private Integer article_pk;
    private String  article_title;
    private String  article_titleimg;
    private String  article_ftitle;
    private String  article_createtime;
    private String  article_sendtime;
    private String  article_author;
    private String  article_source;
    private String  article_istop;
    private String  article_state;
    private String  article_content;
    private String  article_type;
    private String  article_click;
    private String  article_url;
    private String  article_upauthor;
    private String  article_uptime;
    private String  channel_pks;
    public Integer getArticle_pk() {
        return article_pk;
    }

    public void setArticle_pk(Integer article_pk) {
        this.article_pk = article_pk;
    }

    public String getArticle_title() {
        return article_title;
    }

    public void setArticle_title(String article_title) {
        this.article_title = article_title;
    }

    public String getArticle_titleimg() {
        return article_titleimg;
    }

    public void setArticle_titleimg(String article_titleimg) {
        this.article_titleimg = article_titleimg;
    }

    public String getArticle_ftitle() {
        return article_ftitle;
    }

    public void setArticle_ftitle(String article_ftitle) {
        this.article_ftitle = article_ftitle;
    }

    public String getArticle_createtime() {
        return article_createtime;
    }

    public void setArticle_createtime(String article_createtime) {
        this.article_createtime = article_createtime;
    }

    public String getArticle_sendtime() {
        return article_sendtime;
    }

    public void setArticle_sendtime(String article_sendtime) {
        this.article_sendtime = article_sendtime;
    }

    public String getArticle_author() {
        return article_author;
    }

    public void setArticle_author(String article_author) {
        this.article_author = article_author;
    }

    public String getArticle_source() {
        return article_source;
    }

    public void setArticle_source(String article_source) {
        this.article_source = article_source;
    }

    public String getArticle_istop() {
        return article_istop;
    }

    public void setArticle_istop(String article_istop) {
        this.article_istop = article_istop;
    }

    public String getArticle_state() {
        return article_state;
    }

    public void setArticle_state(String article_state) {
        this.article_state = article_state;
    }

    public String getArticle_content() {
        return article_content;
    }

    public void setArticle_content(String article_content) {
        this.article_content = article_content;
    }

    public String getArticle_type() {
        return article_type;
    }

    public void setArticle_type(String article_type) {
        this.article_type = article_type;
    }

    public String getArticle_click() {
        return article_click;
    }

    public void setArticle_click(String article_click) {
        this.article_click = article_click;
    }

    public String getArticle_url() {
        return article_url;
    }

    public void setArticle_url(String article_url) {
        this.article_url = article_url;
    }

    public String getArticle_upauthor() {
        return article_upauthor;
    }

    public void setArticle_upauthor(String article_upauthor) {
        this.article_upauthor = article_upauthor;
    }

    public String getArticle_uptime() {
        return article_uptime;
    }

    public void setArticle_uptime(String article_uptime) {
        this.article_uptime = article_uptime;
    }

    public String getChannel_pks() {
        return channel_pks;
    }

    public void setChannel_pks(String channel_pks) {
        this.channel_pks = channel_pks;
    }
}
